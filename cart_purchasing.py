from flask import Flask
from flask_restful import Api, Resource, reqparse
import mysql.connector

app = Flask(__name__)
api = Api(app)


# To Connect cart_purchasing to cart_purchasing_cart #
def connect_cart():
    # MYSQL database config for connection
    mysql_config = {
        'user': 'root',
        'password': 'root',
        'host': 'cart_purchasing_db',
        'database': 'carts'  # connect to cart db
    }

    # If database is not accepting connections, keep trying until available
    connected = False
    while connected is False:
        try:
            con = mysql.connector.connect(**mysql_config)
            connected = True

        except Exception:
            continue

    return con


class AddToCart(Resource):
    def post(self):
        con = connect_cart()

        cursor = con.cursor(dictionary=True)

        parser = reqparse.RequestParser()
        parser.add_argument('userID', type=int, help='User ID')
        parser.add_argument('itemID', type=str, help='Item ID')
        parser.add_argument('itemPrice', type=str, help='Item Price')
        parser.add_argument('itemName', type=str, help='Item Name')

        args = parser.parse_args()

        userID = args['userID']
        itemID = args['itemID']
        itemPrice = args['itemPrice']
        itemName = args['itemName']

        sql = "INSERT INTO cart (userID, itemID, itemPrice, itemName) VALUES (%s, %s, %s, %s);"

        try:
            cursor.execute(sql, (userID, itemID, itemPrice, itemName))
            con.commit()
        except Exception as e:
            return {'error': str(e)}

        cursor.close()
        con.close()

        return


class DeleteItem(Resource):
    def post(self):
        con = connect_cart()
        cursor = con.cursor(dictionary=True)

        parser = reqparse.RequestParser()
        parser.add_argument('userID', type=int, help='User ID')
        parser.add_argument('itemID', type=str, help='Item ID')
        args = parser.parse_args()

        itemID = args['itemID']
        userID = args['userID']

        sql = "DELETE FROM cart WHERE userID = %s AND itemID = %s;"

        try:
            cursor.execute(sql, (userID, itemID))
            con.commit()
        except Exception as e:
            return {'error': str(e)}

        cursor.close()
        con.close()
        return


class DeleteUserCart(Resource):
    def post(self):
        con = connect_cart()
        cursor = con.cursor(dictionary=True)

        parser = reqparse.RequestParser()
        parser.add_argument('userID', type=int, help='User ID')
        args = parser.parse_args()

        userID = args['userID']

        sql = "DELETE FROM cart WHERE userID = %s;"

        try:
            cursor.execute(sql, (userID,))
            con.commit()
        except Exception as e:
            return {'error': str(e)}

        cursor.close()
        con.close()

        return


class GetCartList(Resource):
    def post(self):
        con = connect_cart()
        cursor = con.cursor(dictionary=True)

        parser = reqparse.RequestParser()
        parser.add_argument('userID', type=int, help='User ID')
        args = parser.parse_args()

        userID = args['userID']

        sql = "SELECT userID, itemID, itemPrice, itemName FROM cart WHERE userID = %s"

        try:
            cursor.execute(sql, (userID,))
        except Exception as e:
            return {'error': str(e)}

        users_cart = cursor.fetchall()
        cursor.close()
        con.close()

        return users_cart

api.add_resource(AddToCart, '/add_to_cart')
api.add_resource(DeleteItem, '/delete_item')
api.add_resource(DeleteUserCart, '/delete_cart')
api.add_resource(GetCartList, '/cart_list')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5004)
