FROM python:3

EXPOSE 5004

RUN mkdir /cart_purchasing
WORKDIR /cart_purchasing

COPY ./requirements.txt /cart_purchasing/requirements_cart.txt
RUN pip install -r requirements_cart.txt

COPY . /cart_purchasing
CMD ["python", "cart_purchasing.py"]